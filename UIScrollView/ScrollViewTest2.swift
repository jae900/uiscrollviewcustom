//
//  ScrollViewTest2.swift
//  UIScrollView
//
//  Created by Jae Ki Lee on 24/08/2019.
//  Copyright © 2019 Jae Ki Lee. All rights reserved.
//

import UIKit

class ScrollViewTest2: UIViewController {
    
    //MARK: - SuperView 안의 객체들
    let scrollView = UIScrollView()
    let contentView = UIView()
    
    let customNavigaionView: UIView = {
        let view = UIView()
        view.backgroundColor = .green
        return view
    }()
    
    let navigationCancelButtonItem: UIButton = {
        let button = UIButton()
        button.backgroundColor = .yellow
        button.addTarget(self, action: #selector(navigaionCancelButtonItemTapped), for: .touchUpInside)
        return button
    }()
    
    @objc func navigaionCancelButtonItemTapped() {
        print("navigaionCancelButtonItemTapped")
    }
    
    let navigationResetButtonItem: UIButton = {
        let button = UIButton()
        button.backgroundColor = .yellow
        button.addTarget(self, action: #selector(navigationResetButtonItemTapped), for: .touchUpInside)
        return button
    }()
    
    @objc func navigationResetButtonItemTapped() {
        print("navigationResetButtonItemTapped")
    }
    
    let largeNavigationCustomLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .lightGray
        return label
    }()
    
    let filteringButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .yellow
        return button
    }()
    
    //MARK: - ScrollView 안의 객체들
    //MARK: - 트랙 테이블뷰
    let beginingTrackTableCellId = "beginingTrackCellId"
    let beginingTrackTableView: UITableView = {
       let tableView = UITableView()
        tableView.isHidden = true
        return tableView
    }()
    
    let endingTrackTableCellId = "endingTrackCellId"
    let endingTrackTableView: UITableView = {
        let tableView = UITableView()
        tableView.isHidden = true
        return tableView
    }()
    
    //MARK: - 단어 틀린 횟수
    let failCountFilterLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .lightGray
        return label
    }()
    
    let moreThanOneFailButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .lightGray
        return button
    }()
    
    let moreThanThreeFailButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .lightGray
        return button
    }()
    
    let moreThanSixFailButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .lightGray
        return button
    }()
    
    let moreThanNineFailButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .lightGray
        return button
    }()
    
    //MARK: - 트랙 선택
    let selectTrackLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .lightGray
        return label
    }()
    
    let beginingTrakButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .lightGray
        button.addTarget(self, action: #selector(beginingTrakButtonTapped), for: .touchUpInside)
        return button
    }()
    
    @objc func beginingTrakButtonTapped() {
        
        if beginingTrackTableView.isHidden == true {
            self.beginingTrackTableView.isHidden = false
        } else {
            self.beginingTrackTableView.isHidden = true
        }
        
    }
    
    let trackFromLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .lightGray
        return label
    }()
    
    let endingTrackButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .lightGray
        button.addTarget(self, action: #selector(endingTrakButtonTapped), for: .touchUpInside)
        return button
    }()
    
    @objc func endingTrakButtonTapped() {
        if endingTrackTableView.isHidden == true {
            self.endingTrackTableView.isHidden = false
        } else {
            self.endingTrackTableView.isHidden = true
        }
    }
    
    let trackToLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .lightGray
        return label
    }()
    
    
    
    //MARK: - SELF CHECK
    let selfCheckLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .lightGray
        return label
    }()
    
    let knowHaveSeenView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        return view
    }()
    
    let knowHaveSeenCheckBox: UIButton = {
        let button = UIButton()
        button.backgroundColor = .purple
        button.addTarget(self, action: #selector(knowHaveSeenCheckBoxTapped), for: .touchUpInside)
        return button
    }()
    
    @objc func knowHaveSeenCheckBoxTapped() {
        print("knowHaveSeenCheckBoxTapped")
    }
    
    let knowHaveSeenLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .purple
        return label
    }()
    
    let knowHaveMeaningView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        return view
    }()
    
    let knowHaveMeaningCheckBox: UIButton = {
        let button = UIButton()
        button.backgroundColor = .purple
        return button
    }()
    
    let knowHaveMeaningLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .purple
        return label
    }()
    
    let knowHaveSpellingView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        return view
    }()
    
    let knowHaveSpellingCheckBox: UIButton = {
        let button = UIButton()
        button.backgroundColor = .purple
        return button
    }()
    
    let knowHaveSpellingLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .purple
        return label
    }()
    
    let knowHavePronunciationView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        return view
    }()
    
    let knowHavePronunciationCheckBox: UIButton = {
        let button = UIButton()
        button.backgroundColor = .purple
        return button
    }()
    
    let knowHavePronunciationLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .purple
        return label
    }()
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        setupViews()
        setupScrollView()
        setupScrollViewContnet()
        
        setupBegiingTrackTableView()
        setupEndingTrackTableView()
    }
    
    //MARK: - SetupViews
    fileprivate func setupViews() {
        let safeArea = self.view.safeAreaLayoutGuide
        
        self.view.addSubview(customNavigaionView)
        self.view.addSubview(largeNavigationCustomLabel)
        self.view.addSubview(filteringButton)
        
        self.customNavigaionView.anchor(top: safeArea.topAnchor, left: safeArea.leftAnchor, bottom: nil, right: safeArea.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 44)
        self.largeNavigationCustomLabel.anchor(top: customNavigaionView.bottomAnchor, left: safeArea.leftAnchor, bottom: nil, right: safeArea.rightAnchor, paddingTop: 12, paddingLeft: 25, paddingBottom: 0, paddingRight: 25, width: 0, height: 41)
        self.filteringButton.anchor(top: nil, left: safeArea.leftAnchor, bottom: safeArea.bottomAnchor, right: safeArea.rightAnchor, paddingTop: 0, paddingLeft: 25, paddingBottom: 25, paddingRight: 25, width: 0, height: 50)
        
        setupCustomNavigationBar()
        
    }
    
    fileprivate func setupCustomNavigationBar() {
        self.customNavigaionView.addSubview(navigationCancelButtonItem)
        self.customNavigaionView.addSubview(navigationResetButtonItem)
        
        navigationCancelButtonItem.anchor(top: customNavigaionView.topAnchor, left: customNavigaionView.leftAnchor, bottom: customNavigaionView.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 25, paddingBottom: 0, paddingRight: 0, width: 44, height: 0)
        navigationResetButtonItem.anchor(top: customNavigaionView.topAnchor, left: nil, bottom: customNavigaionView.bottomAnchor, right: customNavigaionView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 25, width: 55, height: 0)
    }
    
    //MARK: - SetupScrollView
    fileprivate func setupScrollView() {
        let safeArea = self.view.safeAreaLayoutGuide
        self.scrollView.bounces = false
        self.scrollView.showsVerticalScrollIndicator = false
        //customNavigationH + 두 뷰 사이 거리 + largerNavigationFiler Label
        let scrollViewTopSpacing = 44 + 12 + 41
        let scorllViewBottomSpacing = 25 + 50 + 25
        
        self.view.addSubview(self.scrollView)
        scrollView.anchor(top: safeArea.topAnchor, left: safeArea.leftAnchor, bottom: safeArea.bottomAnchor, right: safeArea.rightAnchor, paddingTop: CGFloat(scrollViewTopSpacing), paddingLeft: 25, paddingBottom: CGFloat(scorllViewBottomSpacing), paddingRight: 25, width: 0, height: 0)
        scrollView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.scrollView.addSubview(contentView)
        contentView.anchor(top: scrollView.topAnchor, left: scrollView.leftAnchor, bottom: scrollView.bottomAnchor, right: scrollView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 542)
        contentView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
    }
    
    //MARK: - SetupScrollViewContnet
    fileprivate func setupScrollViewContnet() {
        
        let viewWidth = self.view.frame.width
        
        //단어틀린횟수
        let failCountButtonStakcView = UIStackView(arrangedSubviews: [moreThanOneFailButton, moreThanThreeFailButton, moreThanThreeFailButton, moreThanSixFailButton, moreThanNineFailButton])
        failCountButtonStakcView.spacing = 10
        failCountButtonStakcView.distribution = .fillEqually

        //트랙선택
        beginingTrakButton.widthAnchor.constraint(equalToConstant: viewWidth * 0.293).isActive = true
        trackFromLabel.widthAnchor.constraint(equalToConstant: viewWidth * 0.072).isActive = true
        endingTrackButton.widthAnchor.constraint(equalToConstant: viewWidth * 0.293).isActive = true
        trackToLabel.widthAnchor.constraint(equalToConstant: viewWidth * 0.072).isActive = true
        let selectTrackStackView = UIStackView(arrangedSubviews: [beginingTrakButton, trackFromLabel, endingTrackButton, trackToLabel])
        selectTrackStackView.spacing = 15
        selectTrackStackView.distribution = .equalSpacing
        
        //SELF CHECK
        let selfCheckStackView = UIStackView(arrangedSubviews: [knowHaveSeenView, knowHaveMeaningView, knowHaveSpellingView, knowHavePronunciationView])
        selfCheckStackView.axis = .vertical
        selfCheckStackView.distribution = .fillEqually
        selfCheckStackView.spacing = 0
        
        self.contentView.addSubview(failCountFilterLabel)
        self.contentView.addSubview(failCountButtonStakcView)
        
        self.contentView.addSubview(selectTrackLabel)
        self.contentView.addSubview(selectTrackStackView)
        
        self.contentView.addSubview(selfCheckLabel)
        self.contentView.addSubview(selfCheckStackView)
        
        //단어틀린횟수
        failCountFilterLabel.anchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, paddingTop: 25, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 21)
        failCountButtonStakcView.anchor(top: failCountFilterLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, paddingTop: 15, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        
        //트랙선택
        selectTrackLabel.anchor(top: failCountButtonStakcView.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, paddingTop: 40, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 21)
        selectTrackStackView.anchor(top: selectTrackLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, paddingTop: 17, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        
        //SELF CHECK
        selfCheckLabel.anchor(top: selectTrackStackView.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, paddingTop: 40, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 21)
        selfCheckStackView.anchor(top: selfCheckLabel.bottomAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, paddingTop: 15, paddingLeft: 0, paddingBottom: 28, paddingRight: 0, width: 0, height: 200)
 
        setupdSelfCheckStackView()
    }
    
    fileprivate func setupdSelfCheckStackView() {
        //본적있음 뷰
        self.knowHaveSeenView.addSubview(knowHaveSeenCheckBox)
        self.knowHaveSeenView.addSubview(knowHaveSeenLabel)
        knowHaveSeenCheckBox.anchor(top: knowHaveSeenView.topAnchor, left: knowHaveSeenView.leftAnchor, bottom: knowHaveSeenView.bottomAnchor, right: nil, paddingTop: 15, paddingLeft: 0, paddingBottom: 15, paddingRight: 0, width: 20, height: 20)
        knowHaveSeenLabel.anchor(top: knowHaveSeenView.topAnchor, left: knowHaveSeenCheckBox.rightAnchor, bottom: knowHaveSeenView.bottomAnchor, right: knowHaveSeenView.rightAnchor, paddingTop: 15, paddingLeft: 15, paddingBottom: 15, paddingRight: 0, width: 0, height: 0)
        
        //뜻을모름 뷰
        self.knowHaveMeaningView.addSubview(knowHaveMeaningCheckBox)
        self.knowHaveMeaningView.addSubview(knowHaveMeaningLabel)
        knowHaveMeaningCheckBox.anchor(top: knowHaveMeaningView.topAnchor, left: knowHaveMeaningView.leftAnchor, bottom: knowHaveMeaningView.bottomAnchor, right: nil, paddingTop: 15, paddingLeft: 0, paddingBottom: 15, paddingRight: 0, width: 20, height: 20)
        knowHaveMeaningLabel.anchor(top: knowHaveMeaningView.topAnchor, left: knowHaveMeaningCheckBox.rightAnchor, bottom: knowHaveMeaningView.bottomAnchor, right: knowHaveMeaningView.rightAnchor, paddingTop: 15, paddingLeft: 15, paddingBottom: 15, paddingRight: 0, width: 0, height: 0)
        
        //철자를모름 뷰
        self.knowHaveSpellingView.addSubview(knowHaveSpellingCheckBox)
        self.knowHaveSpellingView.addSubview(knowHaveSpellingLabel)
        knowHaveSpellingCheckBox.anchor(top: knowHaveSpellingView.topAnchor, left: knowHaveSpellingView.leftAnchor, bottom: knowHaveSpellingView.bottomAnchor, right: nil, paddingTop: 15, paddingLeft: 0, paddingBottom: 15, paddingRight: 0, width: 20, height: 20)
        knowHaveSpellingLabel.anchor(top: knowHaveSpellingView.topAnchor, left: knowHaveSpellingCheckBox.rightAnchor, bottom: knowHaveSpellingView.bottomAnchor, right: knowHaveSpellingView.rightAnchor, paddingTop: 15, paddingLeft: 15, paddingBottom: 15, paddingRight: 0, width: 0, height: 0)
        
        //발음을모름 뷰
        self.knowHavePronunciationView.addSubview(knowHavePronunciationCheckBox)
        self.knowHavePronunciationView.addSubview(knowHavePronunciationLabel)
        knowHavePronunciationCheckBox.anchor(top: knowHavePronunciationView.topAnchor, left: knowHavePronunciationView.leftAnchor, bottom: knowHavePronunciationView.bottomAnchor, right: nil, paddingTop: 15, paddingLeft: 0, paddingBottom: 15, paddingRight: 0, width: 20, height: 20)
        knowHavePronunciationLabel.anchor(top: knowHavePronunciationView.topAnchor, left: knowHavePronunciationCheckBox.rightAnchor, bottom: knowHavePronunciationView.bottomAnchor, right: knowHavePronunciationView.rightAnchor, paddingTop: 15, paddingLeft: 15, paddingBottom: 15, paddingRight: 0, width: 0, height: 0)
    }
    
}

extension ScrollViewTest2: UITableViewDelegate, UITableViewDataSource {
    
    fileprivate func setupBegiingTrackTableView() {
        beginingTrackTableView.register(BeginingTrackCell.self, forCellReuseIdentifier: beginingTrackTableCellId)
        beginingTrackTableView.delegate = self
        beginingTrackTableView.dataSource = self
        
        self.contentView.addSubview(beginingTrackTableView)
        beginingTrackTableView.anchor(top: beginingTrakButton.bottomAnchor, left: beginingTrakButton.leftAnchor, bottom: nil, right: beginingTrakButton.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 200)
    }
    
    fileprivate func setupEndingTrackTableView() {
        beginingTrackTableView.register(EndingTrackCell.self, forCellReuseIdentifier: endingTrackTableCellId)
        endingTrackTableView.delegate = self
        endingTrackTableView.dataSource = self
        
        self.contentView.addSubview(endingTrackTableView)
        endingTrackTableView.anchor(top: endingTrackButton.bottomAnchor, left: endingTrackButton.leftAnchor, bottom: nil, right: endingTrackButton.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 200)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var tableCount = 0
        
        if tableView == beginingTrackTableView {
            tableCount = 8
        } else if tableView == endingTrackTableView {
            tableCount = 8
        }

        return tableCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var trackCell = UITableViewCell()
        
        if tableView == beginingTrackTableView {
            trackCell = beginingTrackTableView.dequeueReusableCell(withIdentifier: beginingTrackTableCellId, for: indexPath) as! BeginingTrackCell
        } else if tableView == endingTrackTableView {
             trackCell = beginingTrackTableView.dequeueReusableCell(withIdentifier: endingTrackTableCellId, for: indexPath) as! EndingTrackCell
        }
        
        return trackCell
    }
    
}

class BeginingTrackCell: UITableViewCell {
    
    let trackCellButton: UIButton = {
        let button = UIButton()
        button.setTitle("TRACK 1", for: .normal)
        button.backgroundColor = .red
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(trackCellButton)
        trackCellButton.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class EndingTrackCell: UITableViewCell {
    
    let trackCellButton: UIButton = {
        let button = UIButton()
        button.setTitle("TRACK 1", for: .normal)
        button.backgroundColor = .red
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(trackCellButton)
        trackCellButton.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
