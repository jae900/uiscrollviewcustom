//
//  ViewConfilictTestController.swift
//  UIScrollView
//
//  Created by Jae Ki Lee on 24/08/2019.
//  Copyright © 2019 Jae Ki Lee. All rights reserved.
//

import UIKit

class ViewConfilictTestController: UIViewController {
    
    let firtView: UIView = {
        let view = UIView()
        view.backgroundColor = .red
        return view
    }()
    
    let secondView: UIView = {
        let view = UIView()
        view.backgroundColor = .blue
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(firtView)
        self.view.addSubview(secondView)
        
        firtView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        secondView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
    }
    
    
}
