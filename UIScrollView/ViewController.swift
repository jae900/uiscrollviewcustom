//
//  ViewController.swift
//  UIScrollView
//
//  Created by Jae Ki Lee on 24/08/2019.
//  Copyright © 2019 Jae Ki Lee. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let scrollView = UIScrollView()
    let contentView = UIView()
    
    let backgroundView: UIView = {
        let view = UIView()
        return view
    }()
    
    let customNavigaionView: UIView = {
        let view = UIView()
        view.backgroundColor = .green
        return view
    }()
    
    let largeNavigationCustomLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .lightGray
        return label
    }()
    
    let filteringButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .yellow
        return button
    }()
    
    let redBox: UIView = {
        let view = UIView()
        view.backgroundColor = .red
        return view
    }()

    let greenBox: UIView = {
        let view = UIView()
        view.backgroundColor = .green
        return view
    }()
    
    func setupScrollView() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        self.backgroundView.addSubview(scrollView)
        scrollView.anchor(top: backgroundView.topAnchor, left: backgroundView.leftAnchor, bottom: filteringButton.bottomAnchor, right: backgroundView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        scrollView.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor).isActive = true
        
        scrollView.addSubview(contentView)
        contentView.anchor(top: scrollView.topAnchor, left: scrollView.leftAnchor, bottom: scrollView.bottomAnchor, right: scrollView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        contentView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        
    }
    
    func setupViews() {
        let safeArea = self.view.safeAreaLayoutGuide
        self.view.addSubview(backgroundView)
        backgroundView.anchor(top: safeArea.topAnchor, left: safeArea.leftAnchor, bottom: safeArea.bottomAnchor, right: safeArea.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        self.backgroundView.addSubview(customNavigaionView)
        self.backgroundView.addSubview(largeNavigationCustomLabel)
        self.backgroundView.addSubview(filteringButton)
        
        customNavigaionView.anchor(top: backgroundView.topAnchor, left: backgroundView.leftAnchor, bottom: nil, right: backgroundView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 44)
        largeNavigationCustomLabel.anchor(top: customNavigaionView.bottomAnchor, left: backgroundView.leftAnchor, bottom: nil, right: backgroundView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 40)
        filteringButton.anchor(top: nil, left: backgroundView.leftAnchor, bottom: backgroundView.bottomAnchor, right: backgroundView.rightAnchor, paddingTop: 0, paddingLeft: 25, paddingBottom: 25, paddingRight: 25, width: 0, height: 50)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        setupViews()
        setupScrollView()
        
    }


}

